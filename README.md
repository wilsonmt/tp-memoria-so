
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">



  <link crossorigin="anonymous" href="https://assets-cdn.github.com/assets/frameworks-49f1e970452082ece91a8cb77754f31a769167f4f9cd527a501b1cafa52bb1b6.css" media="all" rel="stylesheet" />
  <link crossorigin="anonymous" href="https://assets-cdn.github.com/assets/github-ce3ebe5d1dd355f0392e6e28517d527ed818deb403150d4e1d697f4642222988.css" media="all" rel="stylesheet" />


  <link crossorigin="anonymous" href="https://assets-cdn.github.com/assets/site-a59bd8fb44017386caff9fbca36994bddc4e2da35d6b61b41f39734d64cf6bb6.css" media="all" rel="stylesheet" />


  <meta name="viewport" content="width=device-width">

  <title>SO-2017-1/README.md at master · flaviovdf/SO-2017-1 · GitHub</title>
  <link rel="search" type="application/opensearchdescription+xml" href="/opensearch.xml" title="GitHub">
  <link rel="fluid-icon" href="https://github.com/fluidicon.png" title="GitHub">
  <meta property="fb:app_id" content="1401488693436528">


    <meta content="https://avatars2.githubusercontent.com/u/521456?v=3&amp;s=400" property="og:image" /><meta content="GitHub" property="og:site_name" /><meta content="object" property="og:type" /><meta content="flaviovdf/SO-2017-1" property="og:title" /><meta content="https://github.com/flaviovdf/SO-2017-1" property="og:url" /><meta content="SO-2017-1 - Disciplina de Sistemas Operacionais 2017-1" property="og:description" />

  <link rel="assets" href="https://assets-cdn.github.com/">

  <meta name="pjax-timeout" content="1000">

  <meta name="request-id" content="F320:2F62:FED3E2:15CABA5:594128A8" data-pjax-transient>


  <meta name="selected-link" value="repo_source" data-pjax-transient>

  <meta name="google-site-verification" content="KT5gs8h0wvaagLKAVWq8bbeNwnZZK1r1XQysX3xurLU">
<meta name="google-site-verification" content="ZzhVyEFwb7w3e0-uOTltm8Jsck2F5StVihD0exw2fsA">
    <meta name="google-analytics" content="UA-3769691-2">

<meta content="collector.githubapp.com" name="octolytics-host" /><meta content="github" name="octolytics-app-id" /><meta content="https://collector.githubapp.com/github-external/browser_event" name="octolytics-event-url" /><meta content="F320:2F62:FED3E2:15CABA5:594128A8" name="octolytics-dimension-request_id" /><meta content="iad" name="octolytics-dimension-region_edge" /><meta content="iad" name="octolytics-dimension-region_render" />
<meta content="/&lt;user-name&gt;/&lt;repo-name&gt;/blob/show" data-pjax-transient="true" name="analytics-location" />




  <meta class="js-ga-set" name="dimension1" content="Logged Out">




      <meta name="hostname" content="github.com">
  <meta name="user-login" content="">

      <meta name="expected-hostname" content="github.com">
    <meta name="js-proxy-site-detection-payload" content="MzUzNDkwOGFhZGJjY2UyZmEwOTcwNjQxN2NjNmI2MDIyZjFiNjk2ODBhNjk4MTJiMDUzMDkzNTI5MTkyMzAyZnx7InJlbW90ZV9hZGRyZXNzIjoiMTg5LjQwLjg2LjIxIiwicmVxdWVzdF9pZCI6IkYzMjA6MkY2MjpGRUQzRTI6MTVDQUJBNTo1OTQxMjhBOCIsInRpbWVzdGFtcCI6MTQ5NzQ0MjQ3MywiaG9zdCI6ImdpdGh1Yi5jb20ifQ==">


  <meta name="html-safe-nonce" content="a07810efa30e37bb33c1d90e6f505f74d9f7a956">

  <meta http-equiv="x-pjax-version" content="8bdcce760611da3548495ddf59e23177">


      <link href="https://github.com/flaviovdf/SO-2017-1/commits/master.atom" rel="alternate" title="Recent Commits to SO-2017-1:master" type="application/atom+xml">

  <meta name="description" content="SO-2017-1 - Disciplina de Sistemas Operacionais 2017-1">
  <meta name="go-import" content="github.com/flaviovdf/SO-2017-1 git https://github.com/flaviovdf/SO-2017-1.git">

  <meta content="521456" name="octolytics-dimension-user_id" /><meta content="flaviovdf" name="octolytics-dimension-user_login" /><meta content="84364733" name="octolytics-dimension-repository_id" /><meta content="flaviovdf/SO-2017-1" name="octolytics-dimension-repository_nwo" /><meta content="true" name="octolytics-dimension-repository_public" /><meta content="false" name="octolytics-dimension-repository_is_fork" /><meta content="84364733" name="octolytics-dimension-repository_network_root_id" /><meta content="flaviovdf/SO-2017-1" name="octolytics-dimension-repository_network_root_nwo" /><meta content="false" name="octolytics-dimension-repository_explore_github_marketplace_ci_cta_shown" />


    <link rel="canonical" href="https://github.com/flaviovdf/SO-2017-1/blob/master/tp2/README.md" data-pjax-transient>


  <meta name="browser-stats-url" content="https://api.github.com/_private/browser/stats">

  <meta name="browser-errors-url" content="https://api.github.com/_private/browser/errors">

  <link rel="mask-icon" href="https://assets-cdn.github.com/pinned-octocat.svg" color="#000000">
  <link rel="icon" type="image/x-icon" href="https://assets-cdn.github.com/favicon.ico">

<meta name="theme-color" content="#1e2327">



  </head>

  <body class="logged-out env-production page-blob">




  <div class="position-relative js-header-wrapper ">
    <a href="#start-of-content" tabindex="1" class="px-2 py-4 show-on-focus js-skip-to-content">Skip to content</a>
    <div id="js-pjax-loader-bar" class="pjax-loader-bar"><div class="progress"></div></div>







          <header class="site-header js-details-container Details" role="banner">
  <div class="site-nav-container">
    <a class="header-logo-invertocat" href="https://github.com/" aria-label="Homepage" data-ga-click="(Logged out) Header, go to homepage, icon:logo-wordmark">
      <svg aria-hidden="true" class="octicon octicon-mark-github" height="32" version="1.1" viewBox="0 0 16 16" width="32"><path fill-rule="evenodd" d="M8 0C3.58 0 0 3.58 0 8c0 3.54 2.29 6.53 5.47 7.59.4.07.55-.17.55-.38 0-.19-.01-.82-.01-1.49-2.01.37-2.53-.49-2.69-.94-.09-.23-.48-.94-.82-1.13-.28-.15-.68-.52-.01-.53.63-.01 1.08.58 1.23.82.72 1.21 1.87.87 2.33.66.07-.52.28-.87.51-1.07-1.78-.2-3.64-.89-3.64-3.95 0-.87.31-1.59.82-2.15-.08-.2-.36-1.02.08-2.12 0 0 .67-.21 2.2.82.64-.18 1.32-.27 2-.27.68 0 1.36.09 2 .27 1.53-1.04 2.2-.82 2.2-.82.44 1.1.16 1.92.08 2.12.51.56.82 1.27.82 2.15 0 3.07-1.87 3.75-3.65 3.95.29.25.54.73.54 1.48 0 1.07-.01 1.93-.01 2.2 0 .21.15.46.55.38A8.013 8.013 0 0 0 16 8c0-4.42-3.58-8-8-8z"/></svg>
    </a>

    <button class="btn-link float-right site-header-toggle js-details-target" type="button" aria-label="Toggle navigation">
      <svg aria-hidden="true" class="octicon octicon-three-bars" height="24" version="1.1" viewBox="0 0 12 16" width="18"><path fill-rule="evenodd" d="M11.41 9H.59C0 9 0 8.59 0 8c0-.59 0-1 .59-1H11.4c.59 0 .59.41.59 1 0 .59 0 1-.59 1h.01zm0-4H.59C0 5 0 4.59 0 4c0-.59 0-1 .59-1H11.4c.59 0 .59.41.59 1 0 .59 0 1-.59 1h.01zM.59 11H11.4c.59 0 .59.41.59 1 0 .59 0 1-.59 1H.59C0 13 0 12.59 0 12c0-.59 0-1 .59-1z"/></svg>
    </button>

    <div class="site-header-menu">
      <nav class="site-header-nav">
        <a href="/features" class="js-selected-navigation-item nav-item" data-ga-click="Header, click, Nav menu - item:features" data-selected-links="/features /features">
          Features
</a>        <a href="/business" class="js-selected-navigation-item nav-item" data-ga-click="Header, click, Nav menu - item:business" data-selected-links="/business /business/security /business/customers /business">
          Business
</a>        <a href="/explore" class="js-selected-navigation-item nav-item" data-ga-click="Header, click, Nav menu - item:explore" data-selected-links="/explore /trending /trending/developers /integrations /integrations/feature/code /integrations/feature/collaborate /integrations/feature/ship /showcases /explore">
          Explore
</a>            <a href="/marketplace" class="js-selected-navigation-item nav-item" data-ga-click="Header, click, Nav menu - item:marketplace" data-selected-links=" /marketplace">
              Marketplace
</a>        <a href="/pricing" class="js-selected-navigation-item nav-item" data-ga-click="Header, click, Nav menu - item:pricing" data-selected-links="/pricing /pricing/developer /pricing/team /pricing/business-hosted /pricing/business-enterprise /pricing">
          Pricing
</a>      </nav>

      <div class="site-header-actions">
          <div class="header-search scoped-search site-scoped-search js-site-search" role="search">
  <!-- '"` --><!-- </textarea></xmp> --></option></form><form accept-charset="UTF-8" action="/flaviovdf/SO-2017-1/search" class="js-site-search-form" data-scoped-search-url="/flaviovdf/SO-2017-1/search" data-unscoped-search-url="/search" method="get"><div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>
    <label class="form-control header-search-wrapper js-chromeless-input-container">
        <a href="/flaviovdf/SO-2017-1/blob/master/tp2/README.md" class="header-search-scope no-underline">This repository</a>
      <input type="text"
        class="form-control header-search-input js-site-search-focus js-site-search-field is-clearable"
        data-hotkey="s"
        name="q"
        value=""
        placeholder="Search"
        aria-label="Search this repository"
        data-unscoped-placeholder="Search GitHub"
        data-scoped-placeholder="Search"
        autocapitalize="off">
        <input type="hidden" class="js-site-search-type-field" name="type" >
    </label>
</form></div>


          <a class="text-bold site-header-link" href="/login?return_to=%2Fflaviovdf%2FSO-2017-1%2Fblob%2Fmaster%2Ftp2%2FREADME.md" data-ga-click="(Logged out) Header, clicked Sign in, text:sign-in">Sign in</a>
            <span class="text-gray">or</span>
            <a class="text-bold site-header-link" href="/join?source=header-repo" data-ga-click="(Logged out) Header, clicked Sign up, text:sign-up">Sign up</a>
      </div>
    </div>
  </div>
</header>


  </div>

  <div id="start-of-content" class="show-on-focus"></div>

    <div id="js-flash-container">
</div>



  <div role="main">
        <div itemscope itemtype="http://schema.org/SoftwareSourceCode">
    <div id="js-repo-pjax-container" data-pjax-container>






    <div class="pagehead repohead instapaper_ignore readability-menu experiment-repo-nav">
      <div class="container repohead-details-container">

        <ul class="pagehead-actions">
  <li>
      <a href="/login?return_to=%2Fflaviovdf%2FSO-2017-1"
    class="btn btn-sm btn-with-count tooltipped tooltipped-n"
    aria-label="You must be signed in to watch a repository" rel="nofollow">
    <svg aria-hidden="true" class="octicon octicon-eye" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M8.06 2C3 2 0 8 0 8s3 6 8.06 6C13 14 16 8 16 8s-3-6-7.94-6zM8 12c-2.2 0-4-1.78-4-4 0-2.2 1.8-4 4-4 2.22 0 4 1.8 4 4 0 2.22-1.78 4-4 4zm2-4c0 1.11-.89 2-2 2-1.11 0-2-.89-2-2 0-1.11.89-2 2-2 1.11 0 2 .89 2 2z"/></svg>
    Watch
  </a>
  <a class="social-count" href="/flaviovdf/SO-2017-1/watchers"
     aria-label="4 users are watching this repository">
    4
  </a>

  </li>

  <li>
      <a href="/login?return_to=%2Fflaviovdf%2FSO-2017-1"
    class="btn btn-sm btn-with-count tooltipped tooltipped-n"
    aria-label="You must be signed in to star a repository" rel="nofollow">
    <svg aria-hidden="true" class="octicon octicon-star" height="16" version="1.1" viewBox="0 0 14 16" width="14"><path fill-rule="evenodd" d="M14 6l-4.9-.64L7 1 4.9 5.36 0 6l3.6 3.26L2.67 14 7 11.67 11.33 14l-.93-4.74z"/></svg>
    Star
  </a>

    <a class="social-count js-social-count" href="/flaviovdf/SO-2017-1/stargazers"
      aria-label="2 users starred this repository">
      2
    </a>

  </li>

  <li>
      <a href="/login?return_to=%2Fflaviovdf%2FSO-2017-1"
        class="btn btn-sm btn-with-count tooltipped tooltipped-n"
        aria-label="You must be signed in to fork a repository" rel="nofollow">
        <svg aria-hidden="true" class="octicon octicon-repo-forked" height="16" version="1.1" viewBox="0 0 10 16" width="10"><path fill-rule="evenodd" d="M8 1a1.993 1.993 0 0 0-1 3.72V6L5 8 3 6V4.72A1.993 1.993 0 0 0 2 1a1.993 1.993 0 0 0-1 3.72V6.5l3 3v1.78A1.993 1.993 0 0 0 5 15a1.993 1.993 0 0 0 1-3.72V9.5l3-3V4.72A1.993 1.993 0 0 0 8 1zM2 4.2C1.34 4.2.8 3.65.8 3c0-.65.55-1.2 1.2-1.2.65 0 1.2.55 1.2 1.2 0 .65-.55 1.2-1.2 1.2zm3 10c-.66 0-1.2-.55-1.2-1.2 0-.65.55-1.2 1.2-1.2.65 0 1.2.55 1.2 1.2 0 .65-.55 1.2-1.2 1.2zm3-10c-.66 0-1.2-.55-1.2-1.2 0-.65.55-1.2 1.2-1.2.65 0 1.2.55 1.2 1.2 0 .65-.55 1.2-1.2 1.2z"/></svg>
        Fork
      </a>

    <a href="/flaviovdf/SO-2017-1/network" class="social-count"
       aria-label="1 user forked this repository">
      1
    </a>
  </li>
</ul>

        <h1 class="public ">
  <svg aria-hidden="true" class="octicon octicon-repo" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path fill-rule="evenodd" d="M4 9H3V8h1v1zm0-3H3v1h1V6zm0-2H3v1h1V4zm0-2H3v1h1V2zm8-1v12c0 .55-.45 1-1 1H6v2l-1.5-1.5L3 16v-2H1c-.55 0-1-.45-1-1V1c0-.55.45-1 1-1h10c.55 0 1 .45 1 1zm-1 10H1v2h2v-1h3v1h5v-2zm0-10H2v9h9V1z"/></svg>
  <span class="author" itemprop="author"><a href="/flaviovdf" class="url fn" rel="author">flaviovdf</a></span><!--
--><span class="path-divider">/</span><!--
--><strong itemprop="name"><a href="/flaviovdf/SO-2017-1" data-pjax="#js-repo-pjax-container">SO-2017-1</a></strong>

</h1>

      </div>
      <div class="container">

<nav class="reponav js-repo-nav js-sidenav-container-pjax"
     itemscope
     itemtype="http://schema.org/BreadcrumbList"
     role="navigation"
     data-pjax="#js-repo-pjax-container">

  <span itemscope itemtype="http://schema.org/ListItem" itemprop="itemListElement">
    <a href="/flaviovdf/SO-2017-1" class="js-selected-navigation-item selected reponav-item" data-hotkey="g c" data-selected-links="repo_source repo_downloads repo_commits repo_releases repo_tags repo_branches /flaviovdf/SO-2017-1" itemprop="url">
      <svg aria-hidden="true" class="octicon octicon-code" height="16" version="1.1" viewBox="0 0 14 16" width="14"><path fill-rule="evenodd" d="M9.5 3L8 4.5 11.5 8 8 11.5 9.5 13 14 8 9.5 3zm-5 0L0 8l4.5 5L6 11.5 2.5 8 6 4.5 4.5 3z"/></svg>
      <span itemprop="name">Code</span>
      <meta itemprop="position" content="1">
</a>  </span>

    <span itemscope itemtype="http://schema.org/ListItem" itemprop="itemListElement">
      <a href="/flaviovdf/SO-2017-1/issues" class="js-selected-navigation-item reponav-item" data-hotkey="g i" data-selected-links="repo_issues repo_labels repo_milestones /flaviovdf/SO-2017-1/issues" itemprop="url">
        <svg aria-hidden="true" class="octicon octicon-issue-opened" height="16" version="1.1" viewBox="0 0 14 16" width="14"><path fill-rule="evenodd" d="M7 2.3c3.14 0 5.7 2.56 5.7 5.7s-2.56 5.7-5.7 5.7A5.71 5.71 0 0 1 1.3 8c0-3.14 2.56-5.7 5.7-5.7zM7 1C3.14 1 0 4.14 0 8s3.14 7 7 7 7-3.14 7-7-3.14-7-7-7zm1 3H6v5h2V4zm0 6H6v2h2v-2z"/></svg>
        <span itemprop="name">Issues</span>
        <span class="Counter">0</span>
        <meta itemprop="position" content="2">
</a>    </span>

  <span itemscope itemtype="http://schema.org/ListItem" itemprop="itemListElement">
    <a href="/flaviovdf/SO-2017-1/pulls" class="js-selected-navigation-item reponav-item" data-hotkey="g p" data-selected-links="repo_pulls /flaviovdf/SO-2017-1/pulls" itemprop="url">
      <svg aria-hidden="true" class="octicon octicon-git-pull-request" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path fill-rule="evenodd" d="M11 11.28V5c-.03-.78-.34-1.47-.94-2.06C9.46 2.35 8.78 2.03 8 2H7V0L4 3l3 3V4h1c.27.02.48.11.69.31.21.2.3.42.31.69v6.28A1.993 1.993 0 0 0 10 15a1.993 1.993 0 0 0 1-3.72zm-1 2.92c-.66 0-1.2-.55-1.2-1.2 0-.65.55-1.2 1.2-1.2.65 0 1.2.55 1.2 1.2 0 .65-.55 1.2-1.2 1.2zM4 3c0-1.11-.89-2-2-2a1.993 1.993 0 0 0-1 3.72v6.56A1.993 1.993 0 0 0 2 15a1.993 1.993 0 0 0 1-3.72V4.72c.59-.34 1-.98 1-1.72zm-.8 10c0 .66-.55 1.2-1.2 1.2-.65 0-1.2-.55-1.2-1.2 0-.65.55-1.2 1.2-1.2.65 0 1.2.55 1.2 1.2zM2 4.2C1.34 4.2.8 3.65.8 3c0-.65.55-1.2 1.2-1.2.65 0 1.2.55 1.2 1.2 0 .65-.55 1.2-1.2 1.2z"/></svg>
      <span itemprop="name">Pull requests</span>
      <span class="Counter">0</span>
      <meta itemprop="position" content="3">
</a>  </span>

    <a href="/flaviovdf/SO-2017-1/projects" class="js-selected-navigation-item reponav-item" data-selected-links="repo_projects new_repo_project repo_project /flaviovdf/SO-2017-1/projects">
      <svg aria-hidden="true" class="octicon octicon-project" height="16" version="1.1" viewBox="0 0 15 16" width="15"><path fill-rule="evenodd" d="M10 12h3V2h-3v10zm-4-2h3V2H6v8zm-4 4h3V2H2v12zm-1 1h13V1H1v14zM14 0H1a1 1 0 0 0-1 1v14a1 1 0 0 0 1 1h13a1 1 0 0 0 1-1V1a1 1 0 0 0-1-1z"/></svg>
      Projects
      <span class="Counter" >0</span>
</a>


    <div class="reponav-dropdown js-menu-container">
      <button type="button" class="btn-link reponav-item reponav-dropdown js-menu-target " data-no-toggle aria-expanded="false" aria-haspopup="true">
        Insights
        <svg aria-hidden="true" class="octicon octicon-triangle-down v-align-middle text-gray" height="11" version="1.1" viewBox="0 0 12 16" width="8"><path fill-rule="evenodd" d="M0 5l6 6 6-6z"/></svg>
      </button>
      <div class="dropdown-menu-content js-menu-content">
        <div class="dropdown-menu dropdown-menu-sw">
          <a class="dropdown-item" href="/flaviovdf/SO-2017-1/pulse" data-skip-pjax>
            <svg aria-hidden="true" class="octicon octicon-pulse" height="16" version="1.1" viewBox="0 0 14 16" width="14"><path fill-rule="evenodd" d="M11.5 8L8.8 5.4 6.6 8.5 5.5 1.6 2.38 8H0v2h3.6l.9-1.8.9 5.4L9 8.5l1.6 1.5H14V8z"/></svg>
            Pulse
          </a>
          <a class="dropdown-item" href="/flaviovdf/SO-2017-1/graphs" data-skip-pjax>
            <svg aria-hidden="true" class="octicon octicon-graph" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M16 14v1H0V0h1v14h15zM5 13H3V8h2v5zm4 0H7V3h2v10zm4 0h-2V6h2v7z"/></svg>
            Graphs
          </a>
        </div>
      </div>
    </div>
</nav>

      </div>
    </div>

<div class="container new-discussion-timeline experiment-repo-nav">
  <div class="repository-content">



  <a href="/flaviovdf/SO-2017-1/blob/c0a25bc582517da06d7165218564eb0781c5d14b/tp2/README.md" class="d-none js-permalink-shortcut" data-hotkey="y">Permalink</a>

  <!-- blob contrib key: blob_contributors:v21:45988e6059340627aedbd9b202a7b4e0 -->

  <div class="file-navigation js-zeroclipboard-container">

<div class="select-menu branch-select-menu js-menu-container js-select-menu float-left">
  <button class=" btn btn-sm select-menu-button js-menu-target css-truncate" data-hotkey="w"

    type="button" aria-label="Switch branches or tags" aria-expanded="false" aria-haspopup="true">
      <i>Branch:</i>
      <span class="js-select-button css-truncate-target">master</span>
  </button>

  <div class="select-menu-modal-holder js-menu-content js-navigation-container" data-pjax>

    <div class="select-menu-modal">
      <div class="select-menu-header">
        <svg aria-label="Close" class="octicon octicon-x js-menu-close" height="16" role="img" version="1.1" viewBox="0 0 12 16" width="12"><path fill-rule="evenodd" d="M7.48 8l3.75 3.75-1.48 1.48L6 9.48l-3.75 3.75-1.48-1.48L4.52 8 .77 4.25l1.48-1.48L6 6.52l3.75-3.75 1.48 1.48z"/></svg>
        <span class="select-menu-title">Switch branches/tags</span>
      </div>

      <div class="select-menu-filters">
        <div class="select-menu-text-filter">
          <input type="text" aria-label="Filter branches/tags" id="context-commitish-filter-field" class="form-control js-filterable-field js-navigation-enable" placeholder="Filter branches/tags">
        </div>
        <div class="select-menu-tabs">
          <ul>
            <li class="select-menu-tab">
              <a href="#" data-tab-filter="branches" data-filter-placeholder="Filter branches/tags" class="js-select-menu-tab" role="tab">Branches</a>
            </li>
            <li class="select-menu-tab">
              <a href="#" data-tab-filter="tags" data-filter-placeholder="Find a tag…" class="js-select-menu-tab" role="tab">Tags</a>
            </li>
          </ul>
        </div>
      </div>

      <div class="select-menu-list select-menu-tab-bucket js-select-menu-tab-bucket" data-tab-filter="branches" role="menu">

        <div data-filterable-for="context-commitish-filter-field" data-filterable-type="substring">


            <a class="select-menu-item js-navigation-item js-navigation-open selected"
               href="/flaviovdf/SO-2017-1/blob/master/tp2/README.md"
               data-name="master"
               data-skip-pjax="true"
               rel="nofollow">
              <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path fill-rule="evenodd" d="M12 5l-8 8-4-4 1.5-1.5L4 10l6.5-6.5z"/></svg>
              <span class="select-menu-item-text css-truncate-target js-select-menu-filter-text">
                master
              </span>
            </a>
        </div>

          <div class="select-menu-no-results">Nothing to show</div>
      </div>

      <div class="select-menu-list select-menu-tab-bucket js-select-menu-tab-bucket" data-tab-filter="tags">
        <div data-filterable-for="context-commitish-filter-field" data-filterable-type="substring">


        </div>

        <div class="select-menu-no-results">Nothing to show</div>
      </div>

    </div>
  </div>
</div>

    <div class="BtnGroup float-right">
      <a href="/flaviovdf/SO-2017-1/find/master"
            class="js-pjax-capture-input btn btn-sm BtnGroup-item"
            data-pjax
            data-hotkey="t">
        Find file
      </a>
      <button aria-label="Copy file path to clipboard" class="js-zeroclipboard btn btn-sm BtnGroup-item tooltipped tooltipped-s" data-copied-hint="Copied!" type="button">Copy path</button>
    </div>
    <div class="breadcrumb js-zeroclipboard-target">
      <span class="repo-root js-repo-root"><span class="js-path-segment"><a href="/flaviovdf/SO-2017-1"><span>SO-2017-1</span></a></span></span><span class="separator">/</span><span class="js-path-segment"><a href="/flaviovdf/SO-2017-1/tree/master/tp2"><span>tp2</span></a></span><span class="separator">/</span><strong class="final-path">README.md</strong>
    </div>
  </div>



  <div class="commit-tease">
      <span class="float-right">
        <a class="commit-tease-sha" href="/flaviovdf/SO-2017-1/commit/00fc6309dd42fae13d795347c958410094469aa3" data-pjax>
          00fc630
        </a>
        <relative-time datetime="2017-06-12T21:43:33Z">Jun 12, 2017</relative-time>
      </span>
      <div>
        <img alt="" class="avatar" data-canonical-src="https://1.gravatar.com/avatar/048ba90aa96f9251eecd43d059c53a2d?d=https%3A%2F%2Fassets-cdn.github.com%2Fimages%2Fgravatars%2Fgravatar-user-420.png&amp;r=x&amp;s=140" height="20" src="https://camo.githubusercontent.com/bb78128bbda3d84d018f97b95844a65a2906488a/68747470733a2f2f312e67726176617461722e636f6d2f6176617461722f30343862613930616139366639323531656563643433643035396335336132643f643d68747470732533412532462532466173736574732d63646e2e6769746875622e636f6d253246696d6167657325324667726176617461727325324667726176617461722d757365722d3432302e706e6726723d7826733d313430" width="20" />
        <span class="user-mention">flaviovdf@dcc.ufmg.br</span>
          <a href="/flaviovdf/SO-2017-1/commit/00fc6309dd42fae13d795347c958410094469aa3" class="message" data-pjax="true" title="Merge branch &#39;master&#39; of github.com:flaviovdf/SO-2017-1">Merge branch 'master' of github.com:flaviovdf/SO-2017-1</a>
      </div>

    <div class="commit-tease-contributors">
      <button type="button" class="btn-link muted-link contributors-toggle" data-facebox="#blob_contributors_box">
        <strong>1</strong>
         contributor
      </button>

    </div>

    <div id="blob_contributors_box" style="display:none">
      <h2 class="facebox-header" data-facebox-id="facebox-header">Users who have contributed to this file</h2>
      <ul class="facebox-user-list" data-facebox-id="facebox-description">
          <li class="facebox-user-list-item">
            <img alt="@flaviovdf" height="24" src="https://avatars3.githubusercontent.com/u/521456?v=3&amp;s=48" width="24" />
            <a href="/flaviovdf">flaviovdf</a>
          </li>
      </ul>
    </div>
  </div>

  <div class="file">
    <div class="file-header">
  <div class="file-actions">

    <div class="BtnGroup">
      <a href="/flaviovdf/SO-2017-1/raw/master/tp2/README.md" class="btn btn-sm BtnGroup-item" id="raw-url">Raw</a>
        <a href="/flaviovdf/SO-2017-1/blame/master/tp2/README.md" class="btn btn-sm js-update-url-with-hash BtnGroup-item" data-hotkey="b">Blame</a>
      <a href="/flaviovdf/SO-2017-1/commits/master/tp2/README.md" class="btn btn-sm BtnGroup-item" rel="nofollow">History</a>
    </div>


        <button type="button" class="btn-octicon disabled tooltipped tooltipped-nw"
          aria-label="You must be signed in to make or propose changes">
          <svg aria-hidden="true" class="octicon octicon-pencil" height="16" version="1.1" viewBox="0 0 14 16" width="14"><path fill-rule="evenodd" d="M0 12v3h3l8-8-3-3-8 8zm3 2H1v-2h1v1h1v1zm10.3-9.3L12 6 9 3l1.3-1.3a.996.996 0 0 1 1.41 0l1.59 1.59c.39.39.39 1.02 0 1.41z"/></svg>
        </button>
        <button type="button" class="btn-octicon btn-octicon-danger disabled tooltipped tooltipped-nw"
          aria-label="You must be signed in to make or propose changes">
          <svg aria-hidden="true" class="octicon octicon-trashcan" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path fill-rule="evenodd" d="M11 2H9c0-.55-.45-1-1-1H5c-.55 0-1 .45-1 1H2c-.55 0-1 .45-1 1v1c0 .55.45 1 1 1v9c0 .55.45 1 1 1h7c.55 0 1-.45 1-1V5c.55 0 1-.45 1-1V3c0-.55-.45-1-1-1zm-1 12H3V5h1v8h1V5h1v8h1V5h1v8h1V5h1v9zm1-10H2V3h9v1z"/></svg>
        </button>
  </div>

  <div class="file-info">
      300 lines (203 sloc)
      <span class="file-info-divider"></span>
    8.2 KB
  </div>
</div>


  <div id="readme" class="readme blob instapaper_body">
    <article class="markdown-body entry-content" itemprop="text"><h1><a id="user-content-tp2-memória" class="anchor" href="#tp2-memória" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>TP2: Memória</h1>
<p>Data de Entrega: Data da Prova 2</p>
<p>Equipe: Até 2 pessoas</p>
<h2><a id="user-content-parte-1-vmm" class="anchor" href="#parte-1-vmm" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Parte 1: VMM</h2>
<h3><a id="user-content-descrição" class="anchor" href="#descrição" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Descrição</h3>
<p>Uma parte importante da gerência de memória de um kernel é sua política de
reposição de páginas. Quando todas as molduras de página (page frames) estão
ocupadas e é necessário adicionar uma  nova página para atender um page fault,
a política de reposição determina qual das páginas atualmente em memória deve
ser excluída.</p>
<p>Neste TP, iremos estudar a qualidade de algumas políticas de reposição através
de simulação.</p>
<h3><a id="user-content-código-base" class="anchor" href="#código-base" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Código Base</h3>
<p>No link abaixo temos um esqueleto do simulador. No mesmo, você precisa
mudar apenas as políticas de reposição da memória.</p>
<p><a href="https://github.com/flaviovdf/SO-2017-1/blob/master/tp2/esqueletos/vmm/">https://github.com/flaviovdf/SO-2017-1/blob/master/tp2/esqueletos/vmm/</a></p>
<h3><a id="user-content-exemplos" class="anchor" href="#exemplos" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Exemplos</h3>
<p>Será usado um simulador de memória virtual e física. Você deve adicionar ao
código do simulador a implementação das políticas de reposição de páginas que
serão estudadas.</p>
<p>O simulador tem quatro parâmetros:</p>
<ul>
<li>i) o número de páginas da memória virtual (lido do arquivo de entrada);</li>
<li>ii) o número de molduras de páginas da memória física (lido do arquivo);</li>
<li>iii) o nome do algoritmo de reposição que será usado; e (linha de comando);</li>
<li>iv) a frequência em que ocorrerão interrupções de relógio
(alguns algoritmos precisam dessa informação) (linha de comando).</li>
</ul>
<p>Você deve implementar os algoritmos:</p>
<ul>
<li>FIFO</li>
<li>Second-Chance</li>
<li>NRU</li>
<li>Aging Utilizando 8 bits para contar</li>
</ul>
<h2><a id="user-content-entrada" class="anchor" href="#entrada" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Entrada</h2>
<p>A entrada será composta de arquivos que indicam um endereço virtual de memória
a ser lido. Além disto, é indicado se a operação é de leitura ou escrita.</p>
<table>
<thead>
<tr>
<th>End.</th>
<th>Tipo</th>
</tr>
</thead>
<tbody>
<tr>
<td>numPáginas numMolduras</td>
<td></td>
</tr>
<tr>
<td>1726</td>
<td>r</td>
</tr>
<tr>
<td>1232</td>
<td>w</td>
</tr>
<tr>
<td>...</td>
<td>...</td>
</tr></tbody></table>
<p>Para o método de saída você deve indicar o número de page-faults do algoritmo.
As entradas vão ser lidas de <code>stdin</code>. Note que o simulador inicia lendo
numPáginas. Isso da o tamanho da memória virtual. Depois o mesmo vai ler
numMolduras, indicando o tamanho da memória física.</p>
<p>Todo endereço lido é um inteiro entre <code>[0, numPáginas)</code>. Isto é, não existe
tradução de endereços no simulador.</p>
<h3><a id="user-content-executando-o-código" class="anchor" href="#executando-o-código" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Executando o Código</h3>
<div class="highlight highlight-source-shell"><pre>$ gcc -Wall vmm.c -o vmm
$ ./vmm random 10 <span class="pl-k">&lt;</span> anomaly.dat</pre></div>
<p>O exemplo acima roda o simulador com a política random, um clock a cada 10
instruções, lendo da entdada padrão o <code>anomaly.dat</code>.</p>
<h3><a id="user-content-plotando-resultados" class="anchor" href="#plotando-resultados" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Plotando Resultados</h3>
<p>Ver notebook na pasta de exemplos</p>
<h3><a id="user-content-recursos-interessantes" class="anchor" href="#recursos-interessantes" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Recursos Interessantes</h3>
<ul>
<li><a href="http://www.ntu.edu.sg/home/smitha/ParaCache/Paracache/vm.html">http://www.ntu.edu.sg/home/smitha/ParaCache/Paracache/vm.html</a></li>
</ul>
<h3><a id="user-content-nome-do-executável-final" class="anchor" href="#nome-do-executável-final" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Nome do Executável Final</h3>
<p><code>vmm</code></p>
<h2><a id="user-content-parte-2-ahloka" class="anchor" href="#parte-2-ahloka" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Parte 2: AhLoka!</h2>
<h3><a id="user-content-descrição-1" class="anchor" href="#descrição-1" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Descrição</h3>
<p>Na segunda parte do TP você vai implementar uma biblioteca de gerenciamento
de alocações. Na sua biblioteca você deve usar a função <code>mmap</code> para alocar e
desalocar espaços de memória.</p>
<p>Funções a serem implementadas:</p>
<div class="highlight highlight-source-c"><pre>#<span class="pl-k">ifndef</span> SO605_GC
#<span class="pl-k">define</span> <span class="pl-en">SO605_GC</span>

#<span class="pl-k">define</span> <span class="pl-en">MEMSIZE</span> <span class="pl-c1">4096</span>*<span class="pl-c1">1024</span>*<span class="pl-c1">1024</span>            <span class="pl-c"><span class="pl-c">//</span>Processo tem 4096 MB de memória</span>

<span class="pl-c"><span class="pl-c">//</span>Nó da lista de memória livre</span>
<span class="pl-k">typedef</span> <span class="pl-k">struct</span> free_node {
 <span class="pl-c1">size_t</span> size;
 <span class="pl-k">struct</span> free_node *next;
} <span class="pl-c1">free_node_t</span>;

<span class="pl-c"><span class="pl-c">//</span>Lista de memória alocada. Ponteira para</span>
<span class="pl-c"><span class="pl-c">//</span>o início da lista.</span>
<span class="pl-k">typedef</span> *<span class="pl-c1">free_node_t</span> <span class="pl-c1">mem_free_t</span>;

<span class="pl-k">void</span> *<span class="pl-en">aloca</span>(<span class="pl-c1">size_t</span> size);
<span class="pl-k">void</span> <span class="pl-en">libera</span>(<span class="pl-k">void</span> *ptr);

#<span class="pl-k">endif</span></pre></div>
<p>Cada <code>aloca</code> deve gerencia a memória livre com uma lista encadeada
representando espaços contínuos de memória:</p>
<pre><code>{size, next} -&gt; {size, next} -&gt; {size, next} -&gt; ...
</code></pre>
<p>As funções não devem chamar <code>malloc</code> nem <code>free</code>. As mesmas devem fazer uso de
<code>mmap</code> (Caso de aloca) e <code>munmap</code> (Caso de free).</p>
<div class="highlight highlight-source-c"><pre><span class="pl-c"><span class="pl-c">//</span>Alocando memória com mmap</span>
<span class="pl-c"><span class="pl-c">//</span>PROT_READ: podemos ler do espaço</span>
<span class="pl-c"><span class="pl-c">//</span>PROT_WRITE: podemos escrever no espaço</span>
<span class="pl-c"><span class="pl-c">//</span>MAP_PRIVATE: Copy on write</span>
<span class="pl-c"><span class="pl-c">//</span>MAP_SHARED: Compartilhe memória com outros processos no fork</span>
<span class="pl-en">mmap</span>(init, len, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -<span class="pl-c1">1</span>, <span class="pl-c1">0</span>);</pre></div>
<p>Você pode fazer uso da função <code>sbrk</code> para definir o primeiro endereço de
memória:</p>
<div class="highlight highlight-source-c"><pre><span class="pl-k">void</span> *init = sbrk(<span class="pl-c1">0</span>);</pre></div>
<h3><a id="user-content-entrada-1" class="anchor" href="#entrada-1" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Entrada</h3>
<p>Sua entrada será uma lista de identificadores e tamanhos. Para cada um destes,
você deve alocar a memória. Também vamos fazer operações de free. Os free's
são identificados de acordo com os ids anteriores:</p>
<table>
<thead>
<tr>
<th>ID</th>
<th>Mem Size</th>
<th>Op</th>
</tr>
</thead>
<tbody>
<tr>
<td>nops</td>
<td></td>
<td></td>
</tr>
<tr>
<td>0</td>
<td>512</td>
<td>a</td>
</tr>
<tr>
<td>1</td>
<td>128</td>
<td>a</td>
</tr>
<tr>
<td>0</td>
<td>-1</td>
<td>f</td>
</tr>
<tr>
<td>...</td>
<td>...</td>
<td>...</td>
</tr></tbody></table>
<p>O -1 indica que não é uma operação de alocação. Deve ser ignorado.</p>
<p>A primeira linha indica o esquema de alocação a ser utilizado. Você deve
implementar:</p>
<ol>
<li>Best fit (bf)</li>
<li>Worst fit (wf)</li>
<li>First fit (ff)</li>
<li>Next fit (nf)</li>
</ol>
<p>A segunda linha indica o número de operações que serão realizadas, assim você
pode gerenciar os ids das operações. Tal linha é um int. Os ids são de <code>[0, nops)</code> (no pior caso, fazemos nops alocas distintos sem frees).</p>
<p>Por fim, entrada acima aloca 2 regiões de memória, uma de 512bytes e outra de
128bytes. Após as alocações, a mesma libera 512bytes da região 0.</p>
<p>As entradas vão ser lidas de <code>stdin</code>.</p>
<h3><a id="user-content-saída" class="anchor" href="#saída" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Saída</h3>
<p>Para cada entrada, seu algoritmo deve mensurar a fragmentação, isto é a
quantidade de espaço livre entre blocos de memória. Caso os <code>aloca</code>s passem do
tamanho máximo de memória, definido no header, seu código deve retornar NULL
igual a biblioteca malloc.</p>
<p>A saída é mensurada pela fragmentação externa, podendo ser um número apenas:</p>
<p><a href="https://camo.githubusercontent.com/8d8ff486f93a1f4edfd61789a664a043ab3dacde/687474703a2f2f77696b696d656469612e6f72672f6170692f726573745f76312f6d656469612f6d6174682f72656e6465722f7376672f37313864643937323762336338646233613862373333613935353836303865313666636635343334" target="_blank"><img src="https://camo.githubusercontent.com/8d8ff486f93a1f4edfd61789a664a043ab3dacde/687474703a2f2f77696b696d656469612e6f72672f6170692f726573745f76312f6d656469612f6d6174682f72656e6465722f7376672f37313864643937323762336338646233613862373333613935353836303865313666636635343334" alt="" data-canonical-src="http://wikimedia.org/api/rest_v1/media/math/render/svg/718dd9727b3c8db3a8b733a9558608e16fcf5434" style="max-width:100%;"></a></p>
<p>Então, uma saída possível seria:</p>
<p><code>0.67</code></p>
<h3><a id="user-content-experimentos" class="anchor" href="#experimentos" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Experimentos</h3>
<p>A ser disponibilizado</p>
<h3><a id="user-content-recursos-interessantes-1" class="anchor" href="#recursos-interessantes-1" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Recursos Interessantes</h3>
<ol>
<li><a href="http://pages.cs.wisc.edu/%7Eremzi/OSTEP/vm-freespace.pdf">http://pages.cs.wisc.edu/~remzi/OSTEP/vm-freespace.pdf</a></li>
</ol>
<h3><a id="user-content-nome-do-executável-final-1" class="anchor" href="#nome-do-executável-final-1" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Nome do Executável Final</h3>
<p><code>aloca</code></p>
<h3><a id="user-content-executando" class="anchor" href="#executando" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Executando</h3>
<p>Veja o esqueleto no link:</p>
<p><a href="https://github.com/flaviovdf/SO-2017-1/blob/master/tp2/esqueletos/ahloka/">https://github.com/flaviovdf/SO-2017-1/blob/master/tp2/esqueletos/ahloka/</a></p>
<pre><code>$ ./aloca bf &lt; alocacoes.dat
0.67
</code></pre>
<p>O único parâmetro indica qual algoritmo será utilizado.</p>
<h2><a id="user-content-parte-3-garbage-collection" class="anchor" href="#parte-3-garbage-collection" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Parte 3: Garbage Collection</h2>
<p><strong>Simule</strong> um coletor de memória simples por contagem de referências.  Não se
preocupe com referências cíclicas, não vamos ter casos como esse.  Para
implementar o GC, você vai ler uma entrada similar a anterior. A mesma terá
operações novas de dependencias entre ids de alocação (imagine como referências
entre objetos em C++/Java/Python).</p>
<ol>
<li>Cada <code>aloca</code> cria uma nova referência para o id.</li>
<li>Cada dependencia entre 2 ids cria uma nova referência para o destino</li>
</ol>
<h3><a id="user-content-entrada-2" class="anchor" href="#entrada-2" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Entrada</h3>
<table>
<thead>
<tr>
<th>ID</th>
<th>Mem Size</th>
<th>Op</th>
</tr>
</thead>
<tbody>
<tr>
<td>nops</td>
<td></td>
<td></td>
</tr>
<tr>
<td>0</td>
<td>512</td>
<td>a</td>
</tr>
<tr>
<td>1</td>
<td>128</td>
<td>a</td>
</tr>
<tr>
<td>2</td>
<td></td>
<td>f</td>
</tr>
<tr>
<td>3</td>
<td>0</td>
<td>r</td>
</tr>
<tr>
<td>4</td>
<td>3</td>
<td>r</td>
</tr>
<tr>
<td>4</td>
<td></td>
<td>f</td>
</tr>
<tr>
<td>...</td>
<td>...</td>
<td>...</td>
</tr></tbody></table>
<p>As operações de referência são identificadas por <code>r</code>.</p>
<h3><a id="user-content-funcionamento" class="anchor" href="#funcionamento" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Funcionamento</h3>
<p>Cada free deve liberar o espaço de memória e reduzir por 1 as referências.
Quando qualquer espaço tem tamanho 0, seu GC pode liberar aquele espaço também.</p>
<p>No exemplo acima, liberamos o id (ponteiro) 4. O mesmo tinha uma referência
para 3. Note que 3 nunca foi alocado por <code>a</code>, então neste momento seu contador
é 0. O mesmo pode ser liberado.</p>
<h3><a id="user-content-saída-1" class="anchor" href="#saída-1" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Saída</h3>
<ol>
<li><strong>Assuma que operações r tem uma custo de 4bytes, criar um ponteiro</strong></li>
<li>Indique a quantidade de bytes ainda residentes na memória no fim do
do seu programa.</li>
</ol>
<p>Ex: <code>128</code></p>
<h3><a id="user-content-dicas" class="anchor" href="#dicas" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Dicas</h3>
<p>Embora não seja necessário para ter um TP correto, você pode imaginar que seu
GC vai ser utilizado por uma linguagem de alto nível tipo Python/Go/Java.
Nestes casos, a contagem de referências são feitas ao realizar um <code>=</code></p>
<div class="highlight highlight-source-java"><pre><span class="pl-smi">Object</span> obj <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-smi">Object</span>(); <span class="pl-c"><span class="pl-c">//</span>aloca</span>
<span class="pl-smi">Object</span> new_ref <span class="pl-k">=</span> obj;      <span class="pl-c"><span class="pl-c">//</span>aumenta em 1 a referência</span></pre></div>
<p>Uma forma simples de contar as referências é criar uma função:</p>
<div class="highlight highlight-source-c"><pre><span class="pl-k">void</span> <span class="pl-en">set_ptr</span>(<span class="pl-k">void</span> **ptr, <span class="pl-k">void</span> *object);</pre></div>
<p>Tal função é utilizada para setar as referências e aumentar os contadores.</p>
<p>Em tempo de compilação, o compilador da sua linguagem pode traduzir todos os
<code>=</code> para um <code>set_ptr</code>.</p>
<p>Uma outra função:</p>
<div class="highlight highlight-source-c"><pre><span class="pl-k">void</span> <span class="pl-en">unset_ptr</span>(<span class="pl-k">void</span> **ptr);</pre></div>
<p>Poderia ser utilizada quando se faz <code>= NULL</code>. O post abaixo fala um pouco
de como a contagem é feita em Python:</p>
<p><a href="https://intopython.com/2016/12/13/memory-management/">https://intopython.com/2016/12/13/memory-management/</a></p>
<h3><a id="user-content-nome-do-executável-final-2" class="anchor" href="#nome-do-executável-final-2" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Nome do Executável Final</h3>
<p><code>garbagec</code></p>
</article>
  </div>

  </div>

  <button type="button" data-facebox="#jump-to-line" data-facebox-class="linejump" data-hotkey="l" class="d-none">Jump to Line</button>
  <div id="jump-to-line" style="display:none">
    <!-- '"` --><!-- </textarea></xmp> --></option></form><form accept-charset="UTF-8" action="" class="js-jump-to-line-form" method="get"><div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>
      <input class="form-control linejump-input js-jump-to-line-field" type="text" placeholder="Jump to line&hellip;" aria-label="Jump to line" autofocus>
      <button type="submit" class="btn">Go</button>
</form>  </div>


  </div>
  <div class="modal-backdrop js-touch-events"></div>
</div>


    </div>
  </div>

  </div>


<div class="container site-footer-container">
  <div class="site-footer " role="contentinfo">
    <ul class="site-footer-links float-right">
        <li><a href="https://github.com/contact" data-ga-click="Footer, go to contact, text:contact">Contact GitHub</a></li>
      <li><a href="https://developer.github.com" data-ga-click="Footer, go to api, text:api">API</a></li>
      <li><a href="https://training.github.com" data-ga-click="Footer, go to training, text:training">Training</a></li>
      <li><a href="https://shop.github.com" data-ga-click="Footer, go to shop, text:shop">Shop</a></li>
        <li><a href="https://github.com/blog" data-ga-click="Footer, go to blog, text:blog">Blog</a></li>
        <li><a href="https://github.com/about" data-ga-click="Footer, go to about, text:about">About</a></li>

    </ul>

    <a href="https://github.com" aria-label="Homepage" class="site-footer-mark" title="GitHub">
      <svg aria-hidden="true" class="octicon octicon-mark-github" height="24" version="1.1" viewBox="0 0 16 16" width="24"><path fill-rule="evenodd" d="M8 0C3.58 0 0 3.58 0 8c0 3.54 2.29 6.53 5.47 7.59.4.07.55-.17.55-.38 0-.19-.01-.82-.01-1.49-2.01.37-2.53-.49-2.69-.94-.09-.23-.48-.94-.82-1.13-.28-.15-.68-.52-.01-.53.63-.01 1.08.58 1.23.82.72 1.21 1.87.87 2.33.66.07-.52.28-.87.51-1.07-1.78-.2-3.64-.89-3.64-3.95 0-.87.31-1.59.82-2.15-.08-.2-.36-1.02.08-2.12 0 0 .67-.21 2.2.82.64-.18 1.32-.27 2-.27.68 0 1.36.09 2 .27 1.53-1.04 2.2-.82 2.2-.82.44 1.1.16 1.92.08 2.12.51.56.82 1.27.82 2.15 0 3.07-1.87 3.75-3.65 3.95.29.25.54.73.54 1.48 0 1.07-.01 1.93-.01 2.2 0 .21.15.46.55.38A8.013 8.013 0 0 0 16 8c0-4.42-3.58-8-8-8z"/></svg>
</a>
    <ul class="site-footer-links">
      <li>&copy; 2017 <span title="0.09978s from unicorn-1553579111-dd3s3">GitHub</span>, Inc.</li>
        <li><a href="https://github.com/site/terms" data-ga-click="Footer, go to terms, text:terms">Terms</a></li>
        <li><a href="https://github.com/site/privacy" data-ga-click="Footer, go to privacy, text:privacy">Privacy</a></li>
        <li><a href="https://github.com/security" data-ga-click="Footer, go to security, text:security">Security</a></li>
        <li><a href="https://status.github.com/" data-ga-click="Footer, go to status, text:status">Status</a></li>
        <li><a href="https://help.github.com" data-ga-click="Footer, go to help, text:help">Help</a></li>
    </ul>
  </div>
</div>



  <div id="ajax-error-message" class="ajax-error-message flash flash-error">
    <svg aria-hidden="true" class="octicon octicon-alert" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M8.865 1.52c-.18-.31-.51-.5-.87-.5s-.69.19-.87.5L.275 13.5c-.18.31-.18.69 0 1 .19.31.52.5.87.5h13.7c.36 0 .69-.19.86-.5.17-.31.18-.69.01-1L8.865 1.52zM8.995 13h-2v-2h2v2zm0-3h-2V6h2v4z"/></svg>
    <button type="button" class="flash-close js-flash-close js-ajax-error-dismiss" aria-label="Dismiss error">
      <svg aria-hidden="true" class="octicon octicon-x" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path fill-rule="evenodd" d="M7.48 8l3.75 3.75-1.48 1.48L6 9.48l-3.75 3.75-1.48-1.48L4.52 8 .77 4.25l1.48-1.48L6 6.52l3.75-3.75 1.48 1.48z"/></svg>
    </button>
    You can't perform that action at this time.
  </div>


    <script crossorigin="anonymous" src="https://assets-cdn.github.com/assets/compat-8a4318ffea09a0cdb8214b76cf2926b9f6a0ced318a317bed419db19214c690d.js"></script>
    <script crossorigin="anonymous" src="https://assets-cdn.github.com/assets/frameworks-73720f027bb317fceb118c259275da4be5efa344c246a12341a68c3168ceeaa7.js"></script>

    <script async="async" crossorigin="anonymous" src="https://assets-cdn.github.com/assets/github-d379af1e588755eb10c26c968d64e3b72b32c13ff9dbb890e19c2c3b5ba17c60.js"></script>




  <div class="js-stale-session-flash stale-session-flash flash flash-warn flash-banner d-none">
    <svg aria-hidden="true" class="octicon octicon-alert" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M8.865 1.52c-.18-.31-.51-.5-.87-.5s-.69.19-.87.5L.275 13.5c-.18.31-.18.69 0 1 .19.31.52.5.87.5h13.7c.36 0 .69-.19.86-.5.17-.31.18-.69.01-1L8.865 1.52zM8.995 13h-2v-2h2v2zm0-3h-2V6h2v4z"/></svg>
    <span class="signed-in-tab-flash">You signed in with another tab or window. <a href="">Reload</a> to refresh your session.</span>
    <span class="signed-out-tab-flash">You signed out in another tab or window. <a href="">Reload</a> to refresh your session.</span>
  </div>
  <div class="facebox" id="facebox" style="display:none;">
  <div class="facebox-popup">
    <div class="facebox-content" role="dialog" aria-labelledby="facebox-header" aria-describedby="facebox-description">
    </div>
    <button type="button" class="facebox-close js-facebox-close" aria-label="Close modal">
      <svg aria-hidden="true" class="octicon octicon-x" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path fill-rule="evenodd" d="M7.48 8l3.75 3.75-1.48 1.48L6 9.48l-3.75 3.75-1.48-1.48L4.52 8 .77 4.25l1.48-1.48L6 6.52l3.75-3.75 1.48 1.48z"/></svg>
    </button>
  </div>
</div>


  </body>
</html>

=======
# TP2: Memória

Data de Entrega: Data da Prova 2

Equipe: Até 2 pessoas

## Parte 1: VMM

### Descrição

Uma parte importante da gerência de memória de um kernel é sua política de
reposição de páginas. Quando todas as molduras de página (page frames) estão
ocupadas e é necessário adicionar uma  nova página para atender um page fault,
a política de reposição determina qual das páginas atualmente em memória deve
ser excluída.

Neste TP, iremos estudar a qualidade de algumas políticas de reposição através
de simulação.

### Código Base

No link abaixo temos um esqueleto do simulador. No mesmo, você precisa
mudar apenas as políticas de reposição da memória.

https://github.com/flaviovdf/SO-2017-1/blob/master/tp2/esqueletos/vmm/

### Exemplos

Será usado um simulador de memória virtual e física. Você deve adicionar ao
código do simulador a implementação das políticas de reposição de páginas que
serão estudadas.

O simulador tem quatro parâmetros:

  * i) o número de páginas da memória virtual (lido do arquivo de entrada);
  * ii) o número de molduras de páginas da memória física (lido do arquivo);
  * iii) o nome do algoritmo de reposição que será usado; e (linha de comando);
  * iv) a frequência em que ocorrerão interrupções de relógio
        (alguns algoritmos precisam dessa informação) (linha de comando).

Você deve implementar os algoritmos:

  * FIFO
  * Second-Chance
  * NRU
  * Aging Utilizando 8 bits para contar

## Entrada

A entrada será composta de arquivos que indicam um endereço virtual de memória
a ser lido. Além disto, é indicado se a operação é de leitura ou escrita.

| End.                   | Tipo |
|------------------------|------|
| numPáginas numMolduras |      |
| 1726                   | r    |
| 1232                   | w    |
| ...                    | ...  |


Para o método de saída você deve indicar o número de page-faults do algoritmo.
As entradas vão ser lidas de `stdin`. Note que o simulador inicia lendo
numPáginas. Isso da o tamanho da memória virtual. Depois o mesmo vai ler
numMolduras, indicando o tamanho da memória física.

Todo endereço lido é um inteiro entre `[0, numPáginas)`. Isto é, não existe
tradução de endereços no simulador.

### Executando o Código

```bash
$ gcc -Wall vmm.c -o vmm
$ ./vmm random 10 < anomaly.dat
```
O exemplo acima roda o simulador com a política random, um clock a cada 10
instruções, lendo da entdada padrão o `anomaly.dat`.

### Plotando Resultados

Ver notebook na pasta de exemplos

### Recursos Interessantes

  * http://www.ntu.edu.sg/home/smitha/ParaCache/Paracache/vm.html

### Nome do Executável Final

`vmm`

## Parte 2: AhLoka!

### Descrição

Na segunda parte do TP você vai implementar uma biblioteca de gerenciamento
de alocações. Na sua biblioteca você deve usar a função `mmap` para alocar e
desalocar espaços de memória.

Funções a serem implementadas:

```c
#ifndef SO605_GC
#define SO605_GC

#define MEMSIZE 4096*1024*1024            //Processo tem 4096 MB de memória

//Nó da lista de memória livre
typedef struct free_node {
 size_t size;
 struct free_node *next;
} free_node_t;

//Lista de memória alocada. Ponteira para
//o início da lista.
typedef *free_node_t mem_free_t;

void *aloca(size_t size);
void libera(void *ptr);

#endif
```

Cada `aloca` deve gerencia a memória livre com uma lista encadeada
representando espaços contínuos de memória:

```
{size, next} -> {size, next} -> {size, next} -> ...
```

As funções não devem chamar `malloc` nem `free`. As mesmas devem fazer uso de
`mmap` (Caso de aloca) e `munmap` (Caso de free).

```c
//Alocando memória com mmap
//PROT_READ: podemos ler do espaço
//PROT_WRITE: podemos escrever no espaço
//MAP_PRIVATE: Copy on write
//MAP_SHARED: Compartilhe memória com outros processos no fork
mmap(init, len, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
```

Você pode fazer uso da função `sbrk` para definir o primeiro endereço de
memória:

```c
void *init = sbrk(0);
```

### Entrada

Sua entrada será uma lista de identificadores e tamanhos. Para cada um destes,
você deve alocar a memória. Também vamos fazer operações de free. Os free's
são identificados de acordo com os ids anteriores:

| ID   | Mem Size | Op  |
|------|----------|-----|
| nops |          |     |
| 0    | 512      | a   |
| 1    | 128      | a   |
| 0    | -1       | f   |
| ...  | ...      | ... |

O -1 indica que não é uma operação de alocação. Deve ser ignorado.

A primeira linha indica o esquema de alocação a ser utilizado. Você deve
implementar:

  1. Best fit (bf)
  2. Worst fit (wf)
  3. First fit (ff)
  4. Next fit (nf)

A segunda linha indica o número de operações que serão realizadas, assim você
pode gerenciar os ids das operações. Tal linha é um int. Os ids são de `[0,
nops)` (no pior caso, fazemos nops alocas distintos sem frees).

Por fim, entrada acima aloca 2 regiões de memória, uma de 512bytes e outra de
128bytes. Após as alocações, a mesma libera 512bytes da região 0.

As entradas vão ser lidas de `stdin`.

### Saída

Para cada entrada, seu algoritmo deve mensurar a fragmentação, isto é a
quantidade de espaço livre entre blocos de memória. Caso os `aloca`s passem do
tamanho máximo de memória, definido no header, seu código deve retornar NULL
igual a biblioteca malloc.

A saída é mensurada pela fragmentação externa, podendo ser um número apenas:

![](http://wikimedia.org/api/rest_v1/media/math/render/svg/718dd9727b3c8db3a8b733a9558608e16fcf5434)

Então, uma saída possível seria:

``0.67``

### Experimentos

A ser disponibilizado

### Recursos Interessantes

  1. http://pages.cs.wisc.edu/~remzi/OSTEP/vm-freespace.pdf

### Nome do Executável Final

`aloca`

### Executando

Veja o esqueleto no link:

https://github.com/flaviovdf/SO-2017-1/blob/master/tp2/esqueletos/ahloka/

```
$ ./aloca bf < alocacoes.dat
0.67
```

O único parâmetro indica qual algoritmo será utilizado.

## Parte 3: Garbage Collection

**Simule** um coletor de memória simples por contagem de referências.  Não se
preocupe com referências cíclicas, não vamos ter casos como esse.  Para
implementar o GC, você vai ler uma entrada similar a anterior. A mesma terá
operações novas de dependencias entre ids de alocação (imagine como referências
entre objetos em C++/Java/Python).

  1. Cada `aloca` cria uma nova referência para o id.
  2. Cada dependencia entre 2 ids cria uma nova referência para o destino

### Entrada

| ID   | Mem Size | Op  |
|------|----------|-----|
| nops |          |     |
| 0    | 512      | a   |
| 1    | 128      | a   |
| 2    |          | f   |
| 3    | 0        | r   |
| 4    | 3        | r   |
| 4    |          | f   |
| ...  | ...      | ... |

As operações de referência são identificadas por `r`.

### Funcionamento

Cada free deve liberar o espaço de memória e reduzir por 1 as referências.
Quando qualquer espaço tem tamanho 0, seu GC pode liberar aquele espaço também.

No exemplo acima, liberamos o id (ponteiro) 4. O mesmo tinha uma referência
para 3. Note que 3 nunca foi alocado por `a`, então neste momento seu contador
é 0. O mesmo pode ser liberado.

### Saída

   1. **Assuma que operações r tem uma custo de 4bytes, criar um ponteiro**
   2. Indique a quantidade de bytes ainda residentes na memória no fim do
      do seu programa.

Ex: `128`

### Dicas

Embora não seja necessário para ter um TP correto, você pode imaginar que seu
GC vai ser utilizado por uma linguagem de alto nível tipo Python/Go/Java.
Nestes casos, a contagem de referências são feitas ao realizar um `=`

```java
Object obj = new Object(); //aloca
Object new_ref = obj;      //aumenta em 1 a referência
```

Uma forma simples de contar as referências é criar uma função:

```c
void set_ptr(void **ptr, void *object);
```

Tal função é utilizada para setar as referências e aumentar os contadores.

Em tempo de compilação, o compilador da sua linguagem pode traduzir todos os
`=` para um `set_ptr`.

Uma outra função:

```c
void unset_ptr(void **ptr);
```

Poderia ser utilizada quando se faz `= NULL`. O post abaixo fala um pouco
de como a contagem é feita em Python:

https://intopython.com/2016/12/13/memory-management/

### Nome do Executável Final

`garbagec`
