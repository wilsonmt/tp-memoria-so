# -*- coding: utf8

import matplotlib.pyplot as plt
import pandas as pd


def main():
    in_fpath = 'noloc.dat'
    df = pd.read_csv(in_fpath)

    print(df.groupby(['algorithm']).mean())

    fifo = df[df['algorithm'] == 'fifo']
    random = df[df['algorithm'] == 'random']
    second_chance = df[df['algorithm'] == 'second_chance']
    nru = df[df['algorithm'] == 'nru']
    aging = df[df['algorithm'] == 'aging']

    x_fifo = fifo['cachesize']
    y_fifo = fifo['faults']

    x_rand = random['cachesize']
    y_rand = random['faults']

    x_second = second_chance['cachesize']
    y_second = second_chance['faults']

    x_nru = nru['cachesize']
    y_nru = nru['faults']

    x_aging = aging['cachesize']
    y_aging = aging['faults']

    plt.scatter(x_fifo, y_fifo, label='FIFO')
    plt.scatter(x_rand, y_rand, label='RND')
    plt.scatter(x_second, y_second, label='SCH')
    plt.scatter(x_nru, y_nru, label='NRU')
    plt.scatter(x_aging, y_aging, label='AGN')

    plt.ylabel('# Page Faults')
    plt.xlabel('Cache Size')
    plt.legend()
    plt.savefig('plot-noloc.pdf')


if __name__ == '__main__':
    main()
