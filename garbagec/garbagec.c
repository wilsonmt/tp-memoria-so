// Esqueleto de um código de alocação de memória (novo maloc)
// Feito para a disciplina DCC065 - Sistemas Operacionais (UFMG)
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <unistd.h>

#include "garbagec.h"

// Strategia de alocação como variável global. Feio mas funciona dado o
// esqueleto do TP.
static char *STRATEGY = NULL;

// Caso você utilize a lista fora da memória que vamos alocar a mesma
// terá que ser acessada como uma var global. Global vars in C.
free_list_t *HEAP = NULL;

/*
 * Sempre aloca até estourar o limite, não implementei libera
 */

void print_mem ()
{
        system("clear");
        size_t i, k;
        free_node_t *a = HEAP->head;
        printf("H");

        while(a != NULL) {
                printf("I");
                for (i = 0; i < (a->size)/10; i++)
                        printf("|");
                k = a->free / 10;
                for (i = 0; i < ((k<5000) ? k : 5000); i++)
                        printf(".");

                a = a->next;
        }
        printf("\n");
        sleep(1);
}
void *alwaysGrow(size_t size) {
        assert(HEAP->lastAlloca != NULL);
        free_node_t *lastAlloca = HEAP->lastAlloca;
        // printf("Ultimo free %lu\n", lastAlloca->free);
        // Temos espaço para alocar + o espaço da lista?
        if (lastAlloca->free < sizeof(free_node_t) + size) {
                return NULL;
        }
        // Sim!
        // Novo nó logo após o último aloca.
        // Posicao da alocacao + tamanho alocado + tamanho do cabeçalho
        free_node_t *newNode = (void*)lastAlloca + lastAlloca->size + \
                               sizeof(free_node_t);
        newNode->next = NULL;
        newNode->size = size;
        newNode->free = lastAlloca->free - sizeof(free_node_t) - size;

        // Só crescemos, o espaço anterior não tem memória livre mais.
        lastAlloca->free = 0;
        lastAlloca->next = newNode;
        newNode->prev = lastAlloca;
        HEAP->lastAlloca = newNode; // Atualiza ponteiro para última operação

        // Retornamos o inicio do espaço que alocamos tirando o cabeçalho
        return (void*)newNode + sizeof(free_node_t);
}

void *ff(size_t size)
{
        assert(HEAP->head != NULL);
        free_node_t *firstAlloca = HEAP->head;

        while ((firstAlloca->free < (sizeof(free_node_t) + size)) && \
                /*(firstAlloca != HEAP->lastAlloca) && */(firstAlloca->next != NULL))
                firstAlloca = firstAlloca->next;

        // printf("Primeiro free %lu next addr %p\n", firstAlloca->free, firstAlloca->next);

        if (firstAlloca->free < sizeof(free_node_t) + size) {
                return NULL;
        }

        free_node_t *newNode = (void*)firstAlloca + firstAlloca->size + sizeof(free_node_t);
        newNode->next = firstAlloca->next;
        newNode->size = size;
        newNode->free = firstAlloca->free - sizeof(free_node_t) - size;

        firstAlloca->free = 0;
        firstAlloca->next = newNode;
        newNode->prev = firstAlloca;
        // if (HEAP->lastAlloca < newNode)
                HEAP->lastAlloca = newNode;

        return (void*)newNode + sizeof(free_node_t);
}

void *bf(size_t size)
{
        assert(HEAP->head != NULL);
        free_node_t *anyAlloca = HEAP->head;
        free_node_t *bestAlloca = HEAP->head;
        while (/*anyAlloca != HEAP->lastAlloca && */anyAlloca->next != NULL)
        {
                anyAlloca = anyAlloca->next;
                if (anyAlloca->free < (sizeof(free_node_t) + size))
                        continue;

                if (bestAlloca->free < (sizeof(free_node_t) + size))
                        bestAlloca = anyAlloca;

                if (anyAlloca->free < bestAlloca->free)
                        bestAlloca = anyAlloca;

                if (bestAlloca->free == (sizeof(free_node_t) + size))
                        break;
        }

        // printf("Melhor free %lu next addr %p\n", bestAlloca->free, bestAlloca->next);

        if (bestAlloca->free < sizeof(free_node_t) + size) {
                return NULL;
        }

        free_node_t *newNode = (void*)bestAlloca + bestAlloca->size + sizeof(free_node_t);
        newNode->next = bestAlloca->next;
        newNode->size = size;
        newNode->free = bestAlloca->free - sizeof(free_node_t) - size;

        bestAlloca->free = 0;
        bestAlloca->next = newNode;
        newNode->prev = bestAlloca;
        // if (HEAP->lastAlloca < newNode)
                HEAP->lastAlloca = newNode;

        return (void*)newNode + sizeof(free_node_t);
}

void *wf(size_t size)
{
        assert(HEAP->head != NULL);
        free_node_t *anyAlloca = HEAP->head;
        free_node_t *bigestAlloca = HEAP->head;
        while (/*anyAlloca != HEAP->lastAlloca && */anyAlloca->next != NULL)
        {
                anyAlloca = anyAlloca->next;
                if (anyAlloca->free < (sizeof(free_node_t) + size))
                        continue;

                if (anyAlloca->free > bigestAlloca->free)
                        bigestAlloca = anyAlloca;
        }

        // printf("Maior free %lu next addr %p\n", bigestAlloca->free, bigestAlloca->next);

        if (bigestAlloca->free < sizeof(free_node_t) + size) {
                return NULL;
        }

        free_node_t *newNode = (void*)bigestAlloca + bigestAlloca->size + sizeof(free_node_t);
        newNode->next = bigestAlloca->next;
        newNode->size = size;
        newNode->free = bigestAlloca->free - sizeof(free_node_t) - size;

        bigestAlloca->free = 0;
        bigestAlloca->next = newNode;
        newNode->prev = bigestAlloca;
        // if (HEAP->lastAlloca < newNode)
                HEAP->lastAlloca = newNode;

        return (void*)newNode + sizeof(free_node_t);
}

void *nf(size_t size)
{
        assert(HEAP->lastAlloca != NULL);
        free_node_t *lastAlloca = HEAP->lastAlloca;

        if (lastAlloca->free < sizeof(free_node_t) + size) {
                if (lastAlloca->next != NULL) {
                        lastAlloca = lastAlloca->next;
                        while ((lastAlloca->free < (sizeof(free_node_t) + size)) && \
                               (lastAlloca->next != NULL)) {
                                lastAlloca = lastAlloca->next;
                        }
                }
                if (lastAlloca->free < sizeof(free_node_t) + size) {
                        lastAlloca = HEAP->head;
                        while ((lastAlloca->free < (sizeof(free_node_t) + size)) && \
                               (lastAlloca != HEAP->lastAlloca)/* && (lastAlloca->next != NULL)*/) {
                                lastAlloca = lastAlloca->next;
                        }
                }
        }

        // printf("Ultimo free %lu\n", lastAlloca->free);

        if (lastAlloca->free < sizeof(free_node_t) + size) {
                return NULL;
        }

        free_node_t *newNode = (void*)lastAlloca + lastAlloca->size + \
                               sizeof(free_node_t);
        newNode->next = lastAlloca->next;
        newNode->size = size;
        newNode->free = lastAlloca->free - sizeof(free_node_t) - size;

        lastAlloca->free = 0;
        lastAlloca->next = newNode;
        newNode->prev = lastAlloca;
        HEAP->lastAlloca = newNode;

        return (void*)newNode + sizeof(free_node_t);
}

void *aloca(size_t size) {
        if (strcmp(STRATEGY, "ag") == 0) {
                return alwaysGrow(size);
        }

        if (strcmp(STRATEGY, "ff") == 0) {
                return ff(size);
        }

        if (strcmp(STRATEGY, "bf") == 0) {
                return bf(size);
        }

        if (strcmp(STRATEGY, "wf") == 0) {
                return wf(size);
        }

        if (strcmp(STRATEGY, "nf") == 0) {
                return nf(size);
        }

        return NULL;
}

void libera(void *ptr)
{
        if (ptr == NULL) return;

        free_node_t *metaData = (void*)ptr - sizeof(free_node_t);
        free_node_t *prevData = metaData->prev;
        free_node_t *nextData = metaData->next;

        if (prevData == NULL) return;

        prevData->free += metaData->size + metaData->free + sizeof(free_node_t);

        if (HEAP->lastAlloca == metaData)
                HEAP->lastAlloca = prevData;

        metaData->next = NULL;
        metaData->prev = NULL;
        metaData->refs = 0;

        prevData->next = nextData;
        if (nextData != NULL)
                nextData->prev = prevData;
}

void *referencia(void *ptr) {
        if (ptr == NULL) return ptr;

        free_node_t *metaData = (void*)ptr - sizeof(free_node_t);
        metaData->refs++;

        return ptr;
}

void desref(void *ptr) {
        if (ptr == NULL) return;

        free_node_t *metaData = (void*)ptr - sizeof(free_node_t);
        if (metaData->refs > 0)
                metaData->refs--;

        if (metaData->refs == 0)
                libera(ptr);
}

void gcollector() {

        free_node_t *metaData = HEAP->head;
        free_node_t *auxData;
        while (metaData->next != NULL)
        {
                metaData = metaData->next;
                // printf("%p\n", metaData);
                // printf("%d\n", metaData->refs);
                // printf("%lu\n", metaData->size);
                // printf("%p\n\n", metaData->prev);

                if (metaData != HEAP->head && (metaData->refs == 0 || metaData->size == 0))
                {
                        auxData = metaData;
                        if (metaData->prev != NULL) {
                                metaData = metaData->prev;
                        } else
                        if (metaData->next != NULL) {
                                metaData = metaData->next;
                        } else return;
                        libera((void *) auxData+sizeof(free_node_t));
                }
        }

}

int free_mem()
{
        free_node_t *node;
        size_t total = 0;

        node = HEAP->head;
        while (node != NULL) {
                total += node->free;
                // printf("+%lu = %lu\n", node->free, total);
                node = node->next;
        }

        return total;
}

void run(void **variables) {
        // Vamos iniciar alocando todo o MEMSIZE. Vamos split e merges depois.
        // Vou iniciar o HEAP usando NULL, deixa o SO decidir. Podemos usar sbrk(0)
        // também para sugerir o local inicial.
        HEAP = mmap(NULL, MEMSIZE,
                    PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS,
                    -1, 0);
        assert(HEAP != NULL);
        assert(HEAP != MAP_FAILED);

        // printf("%p\n%p\n ", HEAP, HEAP+MEMSIZE);

        HEAP->head = (void*) HEAP + sizeof(free_list_t);
        HEAP->lastAlloca = HEAP->head;
        HEAP->head->size = 0;
        HEAP->head->free = MEMSIZE - sizeof(free_list_t) - sizeof(free_node_t);
        HEAP->head->next = NULL;
        HEAP->head->prev = NULL;

                int opid; // ID da operação
        int memsize; // Tamanho da alocação
        char optype; // Tipo da operação
        void *addr;
        while (scanf("%d", &opid) == 1) {
                getchar();
                scanf("%d", &memsize);
                getchar();
                scanf("%c", &optype);
                getchar();
                // printf("Alocando %d; %d; %c\n", opid, memsize, optype);
                if (optype == 'a') { // Aloca!
                        addr = aloca(memsize);
                        if (addr == NULL) {
                                gcollector();
                                addr = aloca(memsize);
                                if (addr == NULL) {
                                        printf("mem full\n");
                                        printf("%d\n", free_mem());
                                        munmap(HEAP, MEMSIZE);
                                        exit(1);
                                }
                        }
                        desref(variables[opid]);
                        variables[opid] = addr;
                } else if (optype == 'f') { // Free!
                        addr = variables[opid];
                        libera(addr);
                        variables[opid] = NULL;
                } else if (optype == 'r') { // Referencia!
                        addr = variables[memsize];
                        desref(variables[opid]);
                        variables[opid] = referencia(addr);
                } else {
                        printf("Erro na entrada");
                        munmap(HEAP, MEMSIZE);
                        exit(1);
                }
                // print_mem();                                                    // Imprime a memoria
        }



        printf("%d\n", free_mem());
        munmap(HEAP, MEMSIZE);
}

int main(int argc, char **argv) {
        if (argc < 2) {
                printf("Usage %s <algorithm>\n", argv[0]);
                exit(1);
        }
        STRATEGY = argv[1];

        int nops;
        scanf("%d\n", &nops);
        // printf("%d\n", nops);

        char *algorithms[] = {"ff", "bf", "wf", "nf", "ag"};
        int n_alg = 5;
        int valid = 0;
        for (int i = 0; i < n_alg; i++) {
                if (strcmp(STRATEGY, algorithms[i]) == 0) {
                        valid = 1;
                        break;
                }
        }
        if (valid == 0) {
                printf("Algoritmo inválido: Usage %s <algorithm>\n", argv[0]);
                printf("--onde o algoritmo vem das opções: {ff, bf, wf, nf, ag}\n");
                exit(1);
        }

        // O vetor variables mantem os endereços de ids alocados.
        // É lido ao executarmos uma operação 'f'
        void **variables = (void **) malloc(nops * sizeof(void**));
        assert(variables != NULL);

        for (int i = 0; i < nops; i++)
                variables[i] = NULL;

        run(variables);
        free(variables);
}
